<footer class="shadow-lg bg-light text-dark"> 
        <div class="container p-3 ">
            <div class="row ">
                <div class="col-3">
                    <p class="h6 fw-bolder pb-3">CONTACT</p>
                    <p>2441139</p>
                    <p>info@home.service</p>
                    <p class="fw-semibold">Corporate Address</p>
                    <p>House # 65, Road # 05, Sector - 7,
                        Uttara, Dhaka 1230</p>
                </div>
                <div class="col-2">
                    <p class="h6 fw-bolder pb-3">OTHER PAGES</p>
                    <p>Blog</p>
                    <p>Help</p>
                    <p>Terms of use</p>
                    <p>Privacy Policy</p>
                    <p>Sitemap</p>
                </div>
                <div class="col-2">
                    <p class="h6 fw-bolder pb-3">COMPANY</p>
                    <p>sManager</p>
                    <p>sBusiness</p>
                    <p>sDelivery</p>
                    <p>sBondhu</p>
                </div>
                <div class="col-5">
                    <p class="h6 fw-bolder pb-3">DOWNLOAD OUR APP</p>
                    <p>Tackle your to-do list wherever you are with our mobile app & make your life easy.</p>
                    <img class="img-fluid d-flex flex-column" src="https://i.ibb.co/3R7Zdmm/app-store.png" height="50px" width="150px" alt="" srcset=""> <br>
                    <img class="img-fluid" src="https://i.ibb.co/379znCR/play-store.png" height="50px" width="150px" alt="" srcset="">
                </div>
              </div>
              <hr>
              <div >
                  <p class="text-center">Copyright © 2022 Home service Platform Limited | All Rights Reserved</p>
              </div>
        </div>
    </footer>