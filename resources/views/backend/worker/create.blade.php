



<x-backend.layouts.master>

    <h1 class="mt-4">Workers</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Workers</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Worker Create
            <a class="btn btn-sm btn-primary" href="">List</a>
        </div>
        <div class="card-body">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form action="{{route('worker.store')}}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="mb-3">
                    <label for="category_id" class="form-label">Category</label>
                    <select name="category" id="category_id" class="form-control">
                        <option value="">Select One</option>
                        @foreach ($categories as $key=>$category)
                        <option value="{{$category}}">{{$category}}</option>
                        @endforeach
                    </select>
                    @error('category')
                        <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{ old('name') }}">

                    @error('name')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="phone_num" class="form-label">Phone_number</label>
                    <input name="phone_num" type="tel" class="form-control" id="phone_num" value="{{ old('phone_num') }}">

                    @error('phone_num')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input name="address" type="text" class="form-control" id="address" value="{{ old('address') }}">

                    @error('address')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

               

            

                <div class="mb-3">
                    <label for="rate" class="form-label">Rate per hour</label>
                    <input name="rate" type="text" class="form-control" id="price" value="{{ old('price') }}">
                    @error('price')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <textarea name="description" class="form-control" rows="10" id="description">
                        {{ old('description') }}
                    </textarea>
                    @error('description')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="image" class="form-label">Image</label>
                    <input name="image" type="file" class="form-control" id="image">
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror
                </div>

                

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>

</x-backend.layouts.master>