<x-backend.layouts.master>
    <h1 class="mt-4">Services</h1>


    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            <a class="btn btn-sm btn-info" href="">PDF</a>
            @can('product-trash-list')
            <a class="btn btn-sm btn-info" href="">Trash List</a>
            @endcan
            <a class="btn btn-sm btn-primary" href="{{route('worker.create')}}">Add New</a>
        </div>
        <div class="card-body">

            @if(session('message'))
            <p class="alert alert-success">{{ session('message') }}</p>
            @endif

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Category</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Address</th>  
                        <th>Rate</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($workers as $worker)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $worker->category }}</td>
                        <td>{{ $worker->name }}</td>
                        <td>{{ $worker->phone_num}}</td>
                        <td>{{ $worker->address}}</td>
                        <td>{{ $worker->rate}}Per hour</td>
                        <td>

                         
                            <a class="btn btn-info btn-sm" href="{{route('worker.show', [$worker->id])}}">Show</a>    
                          

                            <a class="btn btn-warning btn-sm" href="">Edit</a>    
                            
                            <form action="" method="POST" 
                                style="display:inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure want to delete ?')">Delete</button>
                            </form>

                        </td>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>