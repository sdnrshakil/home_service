<x-backend.layouts.master>
    <h1 class="mt-4">Workers</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Workers</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Worker Details
            <a class="btn btn-sm btn-primary" href="{{ route('worker.index') }}">List</a>
        </div>
        <div class="card-body">
            <h3>Category: {{ $worker->category ?? 'N/A' }}</h3>
            <h3>Name: {{ $worker->name }}</h3>
            <p>Phone Number: {{ $worker->phone_num }}</p>
            <p>Address: {{ $worker->address }}</p>
            <p>Description: {{ $worker->description }}</p>
            <p>Rate: {{ $worker->rate }}</p>
            <img src="{{ asset('storage/workers/'. $worker->image) }}" />
           
        </div>

    </div>
</x-backend.layouts.master>