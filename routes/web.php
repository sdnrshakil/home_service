<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\WorkerController;
use Illuminate\Support\Facades\Route;


// routes for frontend

Route::get('/', [ServiceController::class, 'homepage'])->name('homepage');

Route::get('/service-details/{id}', [ServiceController::class, 'serviceDetails'])->name('service-details');


Route::get('/ac-service', function () {
    return view('frontend.ac-service');
})->name('ac-service');

Route::get('/applirance-repair', function () {
    return view('frontend.applirance-repair');
})->name('applirance-repair');


//routes for backend


Route::prefix('admin')->middleware(['auth'])->group(function () {
Route::get('/category', [CategoryController::class, 'create'])->name('category.create');
Route::post('/category/store', [CategoryController::class, 'store'])->name('category.store');

Route::get('/category/index', [CategoryController::class, 'index'])->name('category.index');
Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
Route::delete('/category/{category}/destroy', [CategoryController::class, 'destroy'])->name('category.destroy');
Route::post('/category/{category}/update', [CategoryController::class, 'update'])->name('category.update');
Route::get('/deleted-categories', [CategoryController::class, 'trash'])->name('category.trash');


Route::get('/deleted-categories/{id}/restore', [CategoryController::class, 'restore'])->name('category.restore');
Route::delete('/deleted-categories/{id}', [CategoryController::class, 'delete'])->name('category.delete');
    
    
    });


    
    // routes for service
Route::prefix('admin')->group(function () {

Route::get('/service/create', [ServiceController::class, 'create'])->name('service.create');
Route::post('/service/store', [ServiceController::class, 'store'])->name('service.store');
Route::get('/service/index', [ServiceController::class, 'index'])->name('service.index');
Route::get('/service/show/{id}', [ServiceController::class, 'show'])->name('service.show');




    });

    Route::prefix('admin')->group(function () {

        Route::get('/worker/create', [WorkerController::class, 'create'])->name('worker.create');
        Route::post('/worker/store', [WorkerController::class, 'store'])->name('worker.store');
        Route::get('/worker/index', [WorkerController::class, 'index'])->name('worker.index');
        Route::get('/worker/show/{id}', [WorkerController::class, 'show'])->name('worker.show');
        
        
        
        
    });








Route::get('/dashboard', function () {
    return view('backend.dashboard');
})->middleware(['auth'])->name('backend.dashboard');

require __DIR__.'/auth.php';
