<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Worker;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class WorkerController extends Controller
{
    //



    public function create(){




        $categories = Category::pluck('category_title', 'id')->toArray();
        return view('backend.worker.create',compact('categories'));



    }

    public function store(Request $request){
       

 try{
        $workerdata=$request->all();


        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = time() . '.' . $file->getClientOriginalExtension();
            Image::make($request->file('image'))
                ->resize(300, 200)
                ->save(storage_path() . '/app/public/workers/' . $fileName);
                $workerdata['image'] = $fileName;
        }

        // dd($servicedata);

        $worker = Worker::create($workerdata);

        return Redirect()->route('worker.index');

    }

    catch (QueryException $e) 
    
    {
        return redirect()->back()->withInput()->withErrors($e->getMessage());
    }
    }


    public function index()
    {
        
        $workers = Worker::orderBy('id', 'desc')->get();
        return view('backend.worker.index', compact('workers'));
    }

    public function show($id){
 

        $worker= Worker::where('id', $id)->firstOrFail();


         return view('backend.worker.show' , compact('worker'));




    }

   

    


   
}
